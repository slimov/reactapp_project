import "./assets/css/style.css";
import "./assets/vendors/mdi/css/materialdesignicons.min.css";
import "./assets/vendors/css/vendor.bundle.base.css";
import "./App.css";
import NavbarPage from "./Components/NavbarPage";
import SidebarPage from "./Components/SidebarPage";
import FooterPage from "./Components/FooterPage";

import UserPage from "./Views/User/UserPage";
import EmployerPage from "./Views/Employer/EmployerPage";
import ProfilePage from "./Views/Profile/ProfilePage";

import { Route, Routes, Navigate } from "react-router-dom";
import Adduser from "./Views/User/Adduser";
import Addemploye from "./Views/Employer/Addemploye";
import Edituser from "./Views/User/Edituser";
import Editemploye from "./Views/Employer/Editemploye";
import LoginPage from "./Views/Account/LoginPage";
import AddAdmin from "./Views/Account/AddAdmin";
import Addpicture from "./Views/Profile/Addpicture";
function App() {
  return (
    <div class="container-scroller rtl">
      <NavbarPage />
      <div class="container-fluid page-body-wrapper">
        <SidebarPage />
        <div class="main-panel">
          <div class="content-wrapper">
            <Routes>
              <Route path="/User" element={<UserPage />} />
              {/* <Route path="/" element={<Navigate to="User" />} /> */}
              <Route path="/Employer" element={<EmployerPage />} />
              <Route path="/Profile" element={<ProfilePage />} />
              <Route path="/adduser" element={<Adduser />} />
              <Route path="/addemploye" element={<Addemploye />} />
              <Route path="/edituser" element={<Edituser />} />
              <Route path="/editemploye" element={<Editemploye />} />
              <Route path="/" element={<LoginPage />} />
              {/* <Route path="/" element={<AddAdmin />} /> */}
              <Route path="/addpicture" element={<Addpicture />} />
            </Routes>
          </div>
          <FooterPage />
        </div>
      </div>
    </div>
  );
}

export default App;
