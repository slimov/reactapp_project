import logo from "../../assets/images/logo.svg";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { useState } from "react";
export default function LoginPage() {
  const navigate = useNavigate();
  const [token, settoken] = useState("");
  const [phone, setphone] = useState("");
  const [password, setpassword] = useState("");
  // login function
  const login = async (e) => {
    e.preventDefault();
    await axios({
      // Endpoint to send files
      url: "https://nano-dashboard-delta.vercel.app/login",
      withCredentials: true,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        // authorization:
        //   "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJpYXQiOjE2NzMzNTk4MzMsImV4cCI6MTY3MzUzMjYzM30.ZTEf_3Dbj9aglywiCho6Ojl7SjjKv9dQ8IPL8R2Ltko",
      },
      data: {
        phone: phone,
        password: password,
      },
    })
      .then((res) => {
        localStorage.setItem("token", res.data.token);
        navigate("/User");
      })

      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div class="container-scroller rtl">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div
                class="auth-form-light text-left p-5"
                style={{ marginBottom: "170px" }}
              >
                <form class="pt-3">
                  <div class="form-group">
                    <input
                      type="email"
                      class="form-control form-control-lg"
                      id="exampleInputEmail1"
                      placeholder="رقم الهاتف "
                      onChange={(e) => setphone(e.target.value)}
                    />
                  </div>
                  <div class="form-group">
                    <input
                      type="password"
                      class="form-control form-control-lg"
                      id="exampleInputPassword1"
                      placeholder="كلمة السر "
                      onChange={(e) => setpassword(e.target.value)}
                    />
                  </div>
                  <div class="mt-3">
                    <button
                      class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn"
                      onClick={(e) => login(e)}
                    >
                      دخول
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
