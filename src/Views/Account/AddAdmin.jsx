import logo from "../../assets/images/logo.svg";
import { useNavigate } from "react-router-dom";
import LoginPage from "./LoginPage";
export default function AddAdmin() {
  const navigate = useNavigate();

  const login = (e) => {
    e.preventDefault();
    navigate("/login");
  };
  return (
    <div class="container-scroller rtl">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <form class="pt-3">
                  <h4>اضافة مشرف </h4>
                  <div class="form-group">
                    <input
                      type="email"
                      class="form-control form-control-lg"
                      id="exampleInputEmail1"
                      placeholder=" الاسم "
                    />
                  </div>
                  <div class="form-group">
                    <input
                      type="password"
                      class="form-control form-control-lg"
                      id="exampleInputPassword1"
                      placeholder=" اللقب  "
                    />
                  </div>
                  <div class="form-group">
                    <input
                      type="password"
                      class="form-control form-control-lg"
                      id="exampleInputPassword1"
                      placeholder=" رقم الهاتف   "
                    />
                  </div>
                  <div class="form-group">
                    <input
                      type="password"
                      class="form-control form-control-lg"
                      id="exampleInputPassword1"
                      placeholder="  البريد الإلكتروني    "
                    />
                  </div>
                  <div class="mt-3">
                    <button
                      class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn"
                      onClick={(e) => login(e)}
                    >
                      اضافة
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
