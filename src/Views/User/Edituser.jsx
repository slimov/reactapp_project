import React from "react";
import { BsFillPlusCircleFill } from "react-icons/bs";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import img1 from "../../assets/images/faces/face1.jpg";
import { Country, State, City } from "country-state-city";
import { useState } from "react";

import { useEffect } from "react";

const Edituser = () => {
  const [data, setdata] = useState([]);
  const [countries, setcountries] = useState([]);
  const [country, setcountry] = useState("");
  const [statename, setstatename] = useState("");
  const [cities, setcities] = useState([]);
  const [states, setstates] = useState([]);
  const [phone, setphone] = useState("");
  const [date, setDate] = useState(new Date());
  const fetchcountrie = (countrie) => {
    // const state = states.filter((element) => element.countryCode === "ET");
    // setstates(state);
    const ph = countries.find((element) => element.name === countrie);
    setphone(ph.phonecode);
    setcountry(ph.name);
    const st = states.filter((element) => element.countryCode === ph.isoCode);
    console.log("st" + st);
    setstates(st);
    console.log(ph.phonecode);
  };
  const getcountry = () => {
    const ct = Country.getAllCountries();
    setcountries(ct);
  };
  const getstates = () => {
    const st = State.getAllStates();
    setstates(st);
  };
  const getcities = () => {
    const ct = City.getAllCities();
    setcities(ct);
  };

  useEffect(() => {
    getcountry();
    getstates();
    getcities();
  });
  const getuser = async () => {};
  return (
    <div>
      <div>
        <h4> تعديل حريف </h4>
        <div
          className="col-md-10 offset-md-3 shadow bg-white "
          style={{ padding: "20px", marginTop: "20px" }}
        >
          <form>
            <div class="form-group">
              <div
                style={{
                  display: "flex",
                  marginTop: "40px",
                  marginRight: "80px",
                }}
              >
                <div>
                  <label for="exampleInputEmail1"> الاسم</label>
                  <input
                    type="email"
                    class="form-control"
                    id="exampleInputEmail1"
                    style={{ width: "300px", marginLeft: "80px" }}
                  />
                </div>
                <div>
                  <label for="exampleInputEmail1"> اللقب</label>
                  <input
                    type="email"
                    class="form-control"
                    id="exampleInputEmail1"
                    style={{ width: "300px" }}
                  />
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  marginTop: "40px",
                  marginRight: "80px",
                }}
              >
                <div>
                  <label for="exampleInputEmail1" style={{ marginTop: "5px" }}>
                    {" "}
                    العنوان
                  </label>
                  <div style={{ display: "flex" }}>
                    {countries && (
                      <select
                        class="form-select"
                        onChange={(e) => fetchcountrie(e.target.value)}
                      >
                        <option selected hidden disabled>
                          البلد
                        </option>
                        {countries.map((country) => (
                          <option value={country.name}>{country.name}</option>
                        ))}
                      </select>
                    )}
                    {states && (
                      <select
                        class="form-select"
                        onChange={(e) => setstatename(e.target.value)}
                        style={{ marginRight: "20px" }}
                      >
                        <option selected hidden disabled>
                          المدينة
                        </option>
                        {states.map((state) => (
                          <option value={state.name}>{state.name}</option>
                        ))}
                      </select>
                    )}
                  </div>
                </div>
              </div>
              {/* ligne 2 */}
              <div
                style={{
                  display: "flex",
                  marginTop: "40px",
                  marginRight: "80px",
                }}
              >
                <div style={{ display: "" }}>
                  <label for="exampleInputEmail1">الجنس</label>
                  <div
                    class="form-check"
                    style={{ width: "350px", marginRight: "20px" }}
                  >
                    <label class="relative inline-flex cursor-pointer items-center">
                      <input
                        type="checkbox"
                        value=""
                        class="peer sr-only"
                        defaultChecked={false}
                        style={{ marginLeft: "40px" }}
                      />
                      <div class="peer h-6 w-11 rounded-full bg-gray-200 after:absolute after:top-0.5 after:left-[2px] after:h-5 after:w-5 after:rounded-full after:border after:border-gray-300 after:bg-white after:transition-all after:content-[''] peer-checked:bg-blue-600 peer-checked:after:translate-x-full peer-checked:after:border-white peer-focus:ring-4 peer-focus:ring-blue-300 dark:border-gray-600 dark:bg-gray-700 dark:peer-focus:ring-blue-800"></div>
                      <span class="ml-3 text-sm font-medium text-gray-900 dark:text-gray-300">
                        أنثى
                      </span>
                    </label>
                    <label class="relative inline-flex cursor-pointer items-center">
                      <input
                        type="checkbox"
                        value=""
                        class="peer sr-only"
                        defaultChecked={false}
                      />
                      <div class="peer h-6 w-11 rounded-full bg-gray-200 after:absolute after:top-0.5 after:left-[2px] after:h-5 after:w-5 after:rounded-full after:border after:border-gray-300 after:bg-white after:transition-all after:content-[''] peer-checked:bg-blue-600 peer-checked:after:translate-x-full peer-checked:after:border-white peer-focus:ring-4 peer-focus:ring-blue-300 dark:border-gray-600 dark:bg-gray-700 dark:peer-focus:ring-blue-800"></div>
                      <span class="ml-3 text-sm font-medium text-gray-900 dark:text-gray-300">
                        ذكر
                      </span>
                    </label>
                  </div>
                </div>
                <div>
                  <label for="exampleInputEmail1">رقم الهاتف </label>
                  <input
                    type="email"
                    class="form-control"
                    id="exampleInputEmail1"
                    style={{ width: "300px" }}
                  />
                </div>
              </div>
              {/* ligne 2 */}
              <div
                style={{
                  display: "flex",
                  marginTop: "40px",
                  marginRight: "80px",
                }}
              >
                <div>
                  <label for="exampleInputEmail1">
                    {" "}
                    رقم بطاقة التعريف الوطنية{" "}
                  </label>
                  <input
                    type="email"
                    class="form-control"
                    id="exampleInputEmail1"
                    style={{ width: "300px", marginLeft: "80px" }}
                  />
                </div>
                <div class="form-group ">
                  <label for="exampleInputEmail1"> تاريخ الولادة</label>
                  <DatePicker
                    selected={date}
                    onChange={(date) => setDate(date)}
                    class="form-control"
                  />
                </div>
              </div>

              {/* ligne 4 */}

              <div
                style={{
                  display: "flex",
                  marginTop: "40px",
                  marginRight: "80px",
                }}
              >
                <div>
                  <label for="exampleInputEmail1"> الوظيفة</label>
                  <select
                    class="form-select"
                    aria-label="Default select example"
                  >
                    <option value="1" selected>
                      وظيفة1
                    </option>
                    <option value="2">2وظيفة</option>
                    <option value="3">3وظيفة</option>
                  </select>
                </div>
                <div style={{ marginRight: "270px" }}>
                  <label for="exampleInputEmail1"> البريد الالكتروني </label>
                  <input
                    type="email"
                    class="form-control"
                    id="exampleInputEmail1"
                    style={{ width: "300px" }}
                  />
                </div>
              </div>
              {/* ligne 5 */}

              <div
                style={{
                  display: "flex",
                  marginTop: "40px",
                  marginRight: "80px",
                }}
              >
                <div>
                  <label for="exampleInputEmail1"> الخبرة </label>
                  <input
                    type="email"
                    class="form-control"
                    id="exampleInputEmail1"
                    style={{ width: "300px", marginLeft: "80px" }}
                  />
                </div>
                <div>
                  <label for="exampleInputEmail1"> عدد سنوات الخبرة </label>
                  <input
                    type="email"
                    class="form-control"
                    id="exampleInputEmail1"
                    style={{ width: "300px" }}
                  />
                </div>
              </div>
              {/* ligne 5 */}

              <div
                style={{
                  display: "flex",
                  marginTop: "40px",
                  marginRight: "80px",
                }}
              >
                <div style={{ display: "flex" }}>
                  <label for="exampleInputEmail1"> صور العمل </label>
                  <input class="form-control" type="file" />
                  <img
                    src={img1}
                    class="rounded-circle"
                    style={{ width: "40px", marginRight: "20px" }}
                    alt="Avatar"
                  />
                </div>
              </div>
              <div style={{ marginTop: "20px" }}>
                {/* email whatasapp */}
                <div
                  class="form-check"
                  style={{ width: "350px", marginRight: "70px" }}
                >
                  <label class="relative inline-flex cursor-pointer items-center">
                    <input
                      type="checkbox"
                      value=""
                      class="peer sr-only"
                      defaultChecked={true}
                      style={{ marginLeft: "80px" }}
                    />
                    <div class="peer h-6 w-11 rounded-full bg-gray-200 after:absolute after:top-0.5 after:left-[2px] after:h-5 after:w-5 after:rounded-full after:border after:border-gray-300 after:bg-white after:transition-all after:content-[''] peer-checked:bg-blue-600 peer-checked:after:translate-x-full peer-checked:after:border-white peer-focus:ring-4 peer-focus:ring-blue-300 dark:border-gray-600 dark:bg-gray-700 dark:peer-focus:ring-blue-800"></div>
                    <span class="ml-3 text-sm font-medium text-gray-900 dark:text-gray-300">
                      واتساب
                    </span>
                  </label>
                  <label class="relative inline-flex cursor-pointer items-center">
                    <input
                      type="checkbox"
                      value=""
                      class="peer sr-only"
                      defaultChecked={false}
                    />
                    <div class="peer h-6 w-11 rounded-full bg-gray-200 after:absolute after:top-0.5 after:left-[2px] after:h-5 after:w-5 after:rounded-full after:border after:border-gray-300 after:bg-white after:transition-all after:content-[''] peer-checked:bg-blue-600 peer-checked:after:translate-x-full peer-checked:after:border-white peer-focus:ring-4 peer-focus:ring-blue-300 dark:border-gray-600 dark:bg-gray-700 dark:peer-focus:ring-blue-800"></div>
                    <span class="ml-3 text-sm font-medium text-gray-900 dark:text-gray-300">
                      جيمايل
                    </span>
                  </label>
                </div>
              </div>
              {/* ligne3 */}
            </div>
            <div style={{ marginRight: "80px" }}>
              <button className="btn btn-primary"> تعديل </button>
            </div>
            {/* <button type="submit" class="btn btn-primary">
            Submit
          </button> */}
          </form>
        </div>
        {/* activities images  */}
      </div>
    </div>
  );
};

export default Edituser;
