import React from "react";
import img1 from "../../assets/images/faces/face1.jpg";
import img2 from "../../assets/images/faces/face2.jpg";
import img3 from "../../assets/images/faces/face3.jpg";
import img4 from "../../assets/images/faces/face4.jpg";
import { AiFillDelete } from "react-icons/ai";
import { AiTwotoneEdit } from "react-icons/ai";
import { BsFillXCircleFill } from "react-icons/bs";
import { BsCheckCircleFill } from "react-icons/bs";
import { BsFillPlusCircleFill } from "react-icons/bs";
import { BsFillCheckCircleFill } from "react-icons/bs";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
export default function HomePage() {
  const [users, setusers] = useState([]);
  // get users
  // const getusers = async () => {
  //   await axios({
  //     // Endpoint to send files
  //     url: "https://nano-dashboard-delta.vercel.app/getuserlist",
  //     method: "GET",
  //     headers: {
  //       "Content-Type": "application/json",
  //       authorization:
  //         "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJpYXQiOjE2NzMzNTk4MzMsImV4cCI6MTY3MzUzMjYzM30.ZTEf_3Dbj9aglywiCho6Ojl7SjjKv9dQ8IPL8R2Ltko",
  //     },
  //   })
  //     .then((res) => {
  //       setusers(res.data.arr);
  //       console.log(res.data.arr);
  //     })

  //     .catch((err) => {
  //       console.log(err);
  //     });
  // };
  // // delete user
  // const deleteuser = async (e, id) => {
  //   e.preventDefault();
  //   axios({
  //     // Endpoint to send files
  //     url: "https://nano-dashboard-delta.vercel.app/deleteuser",
  //     method: "POST",
  //     headers: {
  //       // "Content-Type": "application/json",
  //       authorization:
  //         "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJpYXQiOjE2NzMzNTk4MzMsImV4cCI6MTY3MzUzMjYzM30.ZTEf_3Dbj9aglywiCho6Ojl7SjjKv9dQ8IPL8R2Ltko",
  //     },
  //     data: {
  //       userid: id,
  //     },
  //   })
  //     .then((res) => {
  //       getusers();
  //     })

  //     .catch((err) => {
  //       console.log(err);
  //     });
  // };
  // handle change

  // useEffect
  useEffect(() => {
    // getusers();
  }, []);
  const gotoprofile = () => {};
  const [show, setshow] = useState(false);
  const navigate = useNavigate();
  const add = (e) => {
    e.preventDefault();
    navigate("/adduser");
  };
  const goprofile = (e) => {
    e.preventDefault();
    navigate("/profile");
  };
  const gotoedit = (e) => {
    e.preventDefault();
    navigate("/edituser");
  };
  return (
    <div class="row">
      <div class="col-12 grid-margin">
        <div class="card">
          <div>
            <button className="btn" onClick={(e) => add(e)}>
              <BsFillPlusCircleFill style={{ fontSize: "30px" }} />
            </button>
          </div>

          <div class="card-body">
            <h4 class="card-title">قائمة الحرفاء</h4>
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th> إسم الحريف </th>
                    <th> البريد الإلكتروني </th>
                    <th> الهاتف </th>
                    <th> بطاقة التعريف الوطنية </th>
                    <th> تاريخ الولادة </th>
                    <th> عدد سنوات الخبرة </th>
                    <th> موثق </th>
                    <th>الجنس </th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {/* {users.map((element) => {
                    return (
                      <>
                        <tr>
                          <td onClick={(e) => goprofile(e)}>
                            <img src={img1} class="me-2" alt="image" />
                            {element.user_firstname}
                            {element.user_lastname}
                          </td>
                          <td>{element.user_email} </td>
                          <td>{element.user_phone}</td>
                          <td> {element.user_phone} </td>
                          <td> {element.user_birthday}</td>
                          <td> {element.user_seniority} </td>

                          <td>
                            {element.verified === false ? (
                              <BsFillXCircleFill />
                            ) : (
                              <BsFillCheckCircleFill />
                            )}
                          </td>
                          <td>{element.user_civility} </td>
                          <td>
                            <button
                              className="btn"
                              onClick={(e) => deleteuser(e, element.user_id)}
                            >
                              <AiFillDelete />
                            </button>
                            <button
                              className="btn"
                              onClick={(e) => gotoedit(e)}
                            >
                              <AiTwotoneEdit />
                            </button>
                          </td>
                        </tr>
                      </>
                    );
                  })} */}

                  {/* <tr>
                    <td onClick={(e) => goprofile(e)}>
                      <img src={img2} class="me-2" alt="image" /> Stella Johnson
                    </td>
                    <td>user@gmail.com </td>
                    <td>011111</td>
                    <td> 000000 </td>
                    <td> 02/02/1970 </td>
                    <td> 15 </td>

                    <td>
                      <BsFillXCircleFill />
                    </td>
                    <td>ذكر </td>
                    <td>
                      <button className="btn">
                        <AiFillDelete />
                      </button>
                      <button className="btn">
                        <AiTwotoneEdit />
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td onClick={(e) => goprofile(e)}>
                      <img src={img3} class="me-2" alt="image" /> Marina Michel
                    </td>
                    <td>user@gmail.com </td>
                    <td>011111</td>
                    <td> 000000 </td>
                    <td> 02/02/1970 </td>
                    <td> 15 </td>

                    <td>
                      <BsFillXCircleFill />
                    </td>
                    <td>أنثى </td>
                    <td>
                      <button className="btn">
                        <AiFillDelete />
                      </button>
                      <button className="btn" onClick={(e) => gotoedit(e)}>
                        <AiTwotoneEdit />
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td onClick={(e) => goprofile(e)}>
                      <img src={img4} class="me-2" alt="image" /> John Doe
                    </td>
                    <td>user@gmail.com </td>
                    <td>011111</td>
                    <td> 000000 </td>
                    <td> 02/02/1970 </td>
                    <td> 15 </td>

                    <td>
                      <BsFillXCircleFill />
                    </td>
                    <td>أنثى </td>
                    <td>
                      <button className="btn">
                        <AiFillDelete />
                      </button>
                      <button className="btn" onClick={(e) => gotoedit(e)}>
                        <AiTwotoneEdit />
                      </button>
                    </td>
                  </tr> */}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
