import React from "react";
import img1 from "../../assets/images/faces/face1.jpg";

const Addpicture = () => {
  return (
    <div>
      <div>
        <h4> إضافة صور النشاط </h4>
        <div
          className="col-md-6 offset-md-3 shadow bg-white "
          style={{ padding: "20px", marginTop: "20px" }}
        >
          <ul class="navbar-nav navbar-nav-right">
            <li class="nav-item nav-profile dropdown">
              <a
                class="nav-link dropdown-toggle"
                id="profileDropdown"
                href="#"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <div class="nav-profile-img">
                  <img src={img1} alt="image" class="rounded-circle" />
                  <span class="availability-status online"></span>
                </div>
                <div class="nav-profile-text">
                  <p class="mb-1 text-black"> صاحب الحساب </p>
                </div>
              </a>
            
            </li>
          </ul>
        </div>
        <div style={{backgroundColor:"red"}}>

        </div>

        {/* activities images  */}
      </div>
    </div>
  );
};

export default Addpicture;
