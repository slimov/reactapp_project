import React from "react";
import { BsFillPlusCircleFill } from "react-icons/bs";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import img1 from "../../assets/images/faces/face1.jpg";
import { useState } from "react";
import { useRef } from "react";
export default function ProfilePage() {
  const [date, setDate] = useState(new Date());
  const inputRef = useRef(null);
  const handleClick = () => {
    // 👇️ open file input box on click of other element
    inputRef.current.click();
  };

  return (
    <div>
      <div className="d-flex">
        <img
          src={img1}
          class="rounded-circle"
          style={{ width: "100px" }}
          alt="Avatar"
          onClick={handleClick}
        />
        {/* <button className="btn">
          <BsFillPlusCircleFill style={{ fontSize: "30px" }} />
        </button> */}
      </div>
      <div
        className="col-md-10 offset-md-3 shadow bg-white "
        style={{ padding: "20px", marginTop: "20px" }}
      >
        <form>
          <div class="form-group">
            <div
              style={{
                display: "flex",
                marginTop: "40px",
                marginRight: "80px",
              }}
            >
              <div>
                <label for="exampleInputEmail1"> الاسم</label>
                <input
                  type="email"
                  class="form-control"
                  id="exampleInputEmail1"
                  style={{ width: "300px", marginLeft: "80px" }}
                />
              </div>
              <div>
                <label for="exampleInputEmail1"> اللقب</label>
                <input
                  type="email"
                  class="form-control"
                  id="exampleInputEmail1"
                  style={{ width: "300px" }}
                />
              </div>
            </div>
            <div
              style={{
                display: "flex",
                marginTop: "40px",
                marginRight: "80px",
              }}
            >
              <div>
                <label for="exampleInputEmail1"> العنوان</label>
                <input
                  type="email"
                  class="form-control"
                  id="exampleInputEmail1"
                  style={{ width: "300px", marginLeft: "80px" }}
                />
              </div>
              <div>
                <label for="exampleInputEmail1">الجنس</label>
                <input
                  type="email"
                  class="form-control"
                  id="exampleInputEmail1"
                  style={{ width: "300px" }}
                />
              </div>
            </div>
            {/* ligne 2 */}
            <div
              style={{
                display: "flex",
                marginTop: "40px",
                marginRight: "80px",
              }}
            >
              <div>
                <label for="exampleInputEmail1"> البريد الالكتروني</label>
                <input
                  type="email"
                  class="form-control"
                  id="exampleInputEmail1"
                  style={{ width: "300px", marginLeft: "80px" }}
                />
              </div>
              <div>
                <label for="exampleInputEmail1">رقم الهاتف </label>
                <input
                  type="email"
                  class="form-control"
                  id="exampleInputEmail1"
                  style={{ width: "300px" }}
                />
              </div>
            </div>
            {/* ligne 2 */}
            <div
              style={{
                display: "flex",
                marginTop: "40px",
                marginRight: "80px",
              }}
            >
              <div>
                <label for="exampleInputEmail1">
                  {" "}
                  رقم بطاقة التعريف الوطنية{" "}
                </label>
                <input
                  type="email"
                  class="form-control"
                  id="exampleInputEmail1"
                  style={{ width: "300px", marginLeft: "80px" }}
                />
              </div>
              <div>
                <label for="exampleInputEmail1"> تاريخ الولادة</label>
                <DatePicker
                  selected={date}
                  onChange={(date) => setDate(date)}
                  class="form-control"
                />
              </div>
            </div>

            {/* ligne 4 */}

            <div
              style={{
                display: "flex",
                marginTop: "40px",
                marginRight: "80px",
              }}
            >
              <div>
                <label for="exampleInputEmail1"> الوظيفة</label>
                <input
                  type="email"
                  class="form-control"
                  id="exampleInputEmail1"
                  style={{ width: "300px", marginLeft: "80px" }}
                />
              </div>
              <div>
                <label for="exampleInputEmail1"> الخبرة </label>
                <input
                  type="email"
                  class="form-control"
                  id="exampleInputEmail1"
                  style={{ width: "300px" }}
                />
              </div>
            </div>
            {/* ligne 5 */}

            <div
              style={{
                display: "flex",
                marginTop: "40px",
                marginRight: "80px",
              }}
            >
              <div>
                <label for="exampleInputEmail1"> التخصص</label>
                <input
                  type="email"
                  class="form-control"
                  id="exampleInputEmail1"
                  style={{ width: "300px", marginLeft: "80px" }}
                />
              </div>
              <div>
                <label for="exampleInputEmail1"> عدد سنوات الخبرة </label>
                <input
                  type="email"
                  class="form-control"
                  id="exampleInputEmail1"
                  style={{ width: "300px" }}
                />
              </div>
            </div>
            {/* ligne3 */}
          </div>

          {/* <button type="submit" class="btn btn-primary">
            Submit
          </button> */}
        </form>
      </div>
      {/* activities images  */}
      <div className="row" style={{ marginTop: "50px" }}>
        <div className="col">
          <div className="card-body">
            <img alt="" src="https://mdbcdn.b-cdn.net/img/new/avatars/2.webp" />
          </div>
        </div>
        <div className="col">
          <div className="card-body">
            <img alt="" src="https://mdbcdn.b-cdn.net/img/new/avatars/2.webp" />
          </div>
        </div>
        <div className="col">
          <div className="card-body">
            <img
              alt=" "
              src="https://mdbcdn.b-cdn.net/img/new/avatars/2.webp"
            />
          </div>
        </div>
        <div className="col">
          <div className="card-body">
            <img alt="" src="https://mdbcdn.b-cdn.net/img/new/avatars/2.webp" />
          </div>
        </div>
        {/* <div className="col">
          <div className="card-body">
            <img alt="" src="https://mdbcdn.b-cdn.net/img/new/avatars/2.webp" />
          </div>
        </div> */}
        <div className="col" style={{ marginTop: "100px" }}>
          <button className="btn">
            <BsFillPlusCircleFill
              style={{ fontSize: "40px" }}
              onClick={handleClick}
            />
          </button>
        </div>
        <input style={{ display: "none" }} ref={inputRef} type="file" />
      </div>
    </div>
  );
}
