import React from "react";
import img1 from "../../assets/images/faces/face1.jpg";
import img2 from "../../assets/images/faces/face2.jpg";
import img3 from "../../assets/images/faces/face3.jpg";
import img4 from "../../assets/images/faces/face4.jpg";
import { AiFillDelete } from "react-icons/ai";
import { AiTwotoneEdit } from "react-icons/ai";
import { BsFillXCircleFill } from "react-icons/bs";
import { BsCheckCircleFill } from "react-icons/bs";
import { BsFillPlusCircleFill } from "react-icons/bs";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { useState } from "react";
import { useEffect } from "react";
export default function EmployerPage() {
  const [employees, setemployees] = useState([]);
  const navigate = useNavigate();

  // // get employees list
  // const getemployees = async () => {
  //   axios({
  //     // Endpoint to send files
  //     url: "https://nano-dashboard-delta.vercel.app/getemployeelist",
  //     method: "GET",
  //     headers: {
  //       // "Content-Type": "application/json",
  //       authorization:
  //         "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJpYXQiOjE2NzMzNTk4MzMsImV4cCI6MTY3MzUzMjYzM30.ZTEf_3Dbj9aglywiCho6Ojl7SjjKv9dQ8IPL8R2Ltko",
  //     },
  //   })
  //     .then((res) => {
  //       setemployees(res.data.arr);
  //       console.log(res.data.arr);
  //     })

  //     .catch((err) => {
  //       console.log(err);
  //     });
  // };
  // // useEffect
  // useEffect(() => {
  //   getemployees();
  // }, []);
  const add = (e) => {
    e.preventDefault();
    navigate("/addemploye");
  };
  const goprofile = (e) => {
    e.preventDefault();
    navigate("/profile");
  };
  const edit = (e) => {
    e.preventDefault();
    navigate("/editemploye");
  };
  return (
    <div class="row">
      <div class="col-12 grid-margin">
        <div class="card">
          <div>
            <button className="btn" onClick={(e) => add(e)}>
              <BsFillPlusCircleFill style={{ fontSize: "30px" }} />
            </button>
          </div>
          <div class="card-body">
            <h4 class="card-title">قائمة العملاء</h4>
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th> إسم العميل </th>
                    <th> البريد الإلكتروني </th>
                    <th> الهاتف </th>
                    <th> المهنة </th>
                    <th>الجنس </th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {/* {employees.map((element) => {
                    return (
                      <>
                        <tr>
                          <td onClick={(e) => goprofile(e)}>
                            <img src={img1} class="me-2" alt="image" />{" "}
                            {element.user_firstname}
                            {element.user_lastname}
                          </td>
                          <td>{element.user_email} </td>
                          <td>{element.user_phone}</td>
                          <td> {element.user_speciality} </td>
                          <td>{element.user_civility} </td>
                          <td>
                            <button className="btn">
                              <AiFillDelete />
                            </button>
                            <button className="btn" onClick={(e) => edit(e)}>
                              <AiTwotoneEdit />
                            </button>
                          </td>
                        </tr>
                      </>
                    );
                  })} */}

                  {/* <tr>
                    <td onClick={(e) => goprofile(e)}>
                      <img src={img2} class="me-2" alt="image" /> Stella Johnson
                    </td>
                    <td>user@gmail.com </td>
                    <td>011111</td>

                    <td> عامل </td>
                    <td>ذكر </td>
                    <td>
                      <button className="btn">
                        <AiFillDelete />
                      </button>
                      <button className="btn" onClick={(e) => edit(e)}>
                        <AiTwotoneEdit />
                      </button>
                    </td>
                  </tr> */}
                  {/* <tr>
                    <td onClick={(e) => goprofile(e)}>
                      <img src={img3} class="me-2" alt="image" /> Marina Michel
                    </td>
                    <td>user@gmail.com </td>
                    <td>011111</td>

                    <td> عامل </td>
                    <td>ذكر </td>

                    <td>
                      <button className="btn">
                        <AiFillDelete />
                      </button>
                      <button className="btn" onClick={(e) => edit(e)}>
                        <AiTwotoneEdit />
                      </button>
                    </td>
                  </tr> */}
                  {/* <tr>
                    <td onClick={(e) => goprofile(e)}>
                      <img src={img4} class="me-2" alt="image" /> John Doe
                    </td>
                    <td>user@gmail.com </td>
                    <td>011111</td>

                    <td> عامل </td>
                    <td>ذكر </td>

                    <td>
                      <button className="btn">
                        <AiFillDelete />
                      </button>
                      <button className="btn">
                        <AiTwotoneEdit />
                      </button>
                    </td>
                  </tr> */}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
