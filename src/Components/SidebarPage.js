import React from "react";
import img1 from "../assets/images/faces/face1.jpg";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import axios from "axios";
export default function SidebarPage() {
  const navigate = useNavigate();
  // logout
  const logout = () => {
    const options = {
      method: "GET",
      url: "https://nano-dashboard-delta.vercel.app/logout",
      headers: {
        "content-type": "application/json",
        // "Authorization": "your-rapidapi-key",
      },
    };
    axios
      .request(options)
      .then(function (response) {
        localStorage.setItem("token", "");
        navigate("/");
      })
      .catch(function (error) {
        console.error(error);
      });
  };

  return (
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
      <ul class="nav">
        <li class="nav-item nav-profile">
          <a href="#" class="nav-link">
            <div class="nav-profile-image">
              <img src={img1} alt="profile" />
              <span class="login-status online"></span>
            </div>
            <div class="nav-profile-text d-flex flex-column">
              <span class="font-weight-bold mb-2">إسم صاحب الحساب هنا</span>
              <span class="text-secondary text-small">يمكنك إضافة وظيفته</span>
            </div>
          </a>
        </li>
        <li class="nav-item">
          <Link to="/User" class="nav-link">
            <span class="menu-title">الحرفاء</span>
            <i class="mdi mdi-home menu-icon"></i>
          </Link>
        </li>
        <li class="nav-item">
          <Link to="/Employer" class="nav-link">
            <span class="menu-title">العملاء</span>
            <i class="mdi mdi-home menu-icon"></i>
          </Link>
        </li>
        <li class="nav-item">
          <Link to="/Profile" class="nav-link">
            <span class="menu-title">الحساب</span>
            <i class="mdi mdi-contacts menu-icon"></i>
          </Link>
        </li>
        <li class="nav-item">
          <button className="btn" onClick={() => logout()}>
            <span class="menu-title">تسجيل الخروج</span>
            <i
              class="mdi mdi-contacts menu-icon"
              style={{ marginRight: "51px" }}
            ></i>
          </button>
        </li>
      </ul>
    </nav>
  );
}
