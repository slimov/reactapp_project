import img1 from "../assets/images/faces/face1.jpg";
import img2 from "../assets/images/faces/face4.jpg";
import img3 from "../assets/images/faces/face3.jpg";
import logo from "../assets/images/logo.svg";
import minilogo from "../assets/images/logo-mini.svg";

import React from "react";
import $ from "jquery";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export default function NavbarPage() {
  const navigate = useNavigate();
  // logout
  
  //fullscreen

  function openFullscreen() {
    if (
      (document.fullScreenElement !== undefined &&
        document.fullScreenElement === null) ||
      (document.msFullscreenElement !== undefined &&
        document.msFullscreenElement === null) ||
      (document.mozFullScreen !== undefined && !document.mozFullScreen) ||
      (document.webkitIsFullScreen !== undefined &&
        !document.webkitIsFullScreen)
    ) {
      if (document.documentElement.requestFullScreen) {
        document.documentElement.requestFullScreen();
      } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
      } else if (document.documentElement.webkitRequestFullScreen) {
        document.documentElement.webkitRequestFullScreen(
          Element.ALLOW_KEYBOARD_INPUT
        );
      } else if (document.documentElement.msRequestFullscreen) {
        document.documentElement.msRequestFullscreen();
      }
    } else {
      if (document.cancelFullScreen) {
        document.cancelFullScreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    }
  }
  var body = $("body");
  function opensider() {
    if (
      body.hasClass("sidebar-toggle-display") ||
      body.hasClass("sidebar-absolute")
    ) {
      body.toggleClass("sidebar-hidden");
    } else {
      body.toggleClass("sidebar-icon-only");
    }
  }

  function openside() {
    $(".sidebar-offcanvas").toggleClass("active");
  }

  return (
    <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item nav-profile dropdown">
            <a
              class="nav-link dropdown-toggle"
              id="profileDropdown"
              href="#"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              <div class="nav-profile-img">
                <img src={img1} alt="image" />
                <span class="availability-status online"></span>
              </div>
              <div class="nav-profile-text">
                <p class="mb-1 text-black">إسم صاحب الحساب هنا</p>
              </div>
            </a>
            <div
              class="dropdown-menu navbar-dropdown"
              aria-labelledby="profileDropdown"
            >
              <a class="dropdown-item" href="#">
                <i class="mdi mdi-cached me-2 text-success"></i> Activity Log{" "}
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">
                <i class="mdi mdi-logout me-2 text-primary"></i> Signout{" "}
              </a>
            </div>
          </li>
        </ul>
        <button
          class="navbar-toggler navbar-toggler-right d-lg-none align-self-center"
          type="button"
          data-toggle="offcanvas"
          onClick={() => openside()}
        >
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
  );
}
